package com.one;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.*;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.pdfbox.ExtractText;
import org.apache.pdfbox.PDFSplit;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdfviewer.MapEntry;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;

/**
 * Created by Ravish on 4/8/2014.
 */

public class DataCollection {


     String directoryLocation = "data";
     String storeLocation =     "out.txt";

    public static void main(String[] args) {


        DataCollection collector = new DataCollection();
      //  File file = new File(collector.directoryLocation);

       Collection<String> files =  collector.readFiles();
       Map<Integer, List<String>> sortedWords = collector.findKeyWords(files);
       collector.writeToFile(collector.storeLocation, sortedWords);





    }


    public Collection<String> readFiles()
    {
        File directory = new File(directoryLocation);

        Collection<File> files = FileUtils.listFiles(directory, new WildcardFileFilter("*.pdf"), null);
        Collection<String> parsedFiles = new ArrayList<String>();
        int i = 0;
        for(File file: files)
        {


            parsedFiles.add(readPDF(file));


        }


        return parsedFiles;

    }


    public Map<Integer, List<String>> findKeyWords(Collection<String> files)
    {

        Map<String, Integer> wordCount = new HashMap<String, Integer>();

        for(String file: files) {
            Scanner scanner = new Scanner(file);

            while (scanner.hasNext())
            {
                String word = scanner.next().toLowerCase();
                wordCount.put(word, wordCount.containsKey(word) ? wordCount.get(word) + 1 : 1);
            }



        }

        Map<Integer, List<String>> sortedWords = new TreeMap<Integer, List<String>>();

        for(Map.Entry<String, Integer> entry: wordCount.entrySet())
        {

            if(!sortedWords.containsKey(entry.getValue()))
            {
                sortedWords.put(entry.getValue(), new ArrayList<String>());

            }

            sortedWords.get(entry.getValue()).add(entry.getKey());


        }
        System.out.println(sortedWords.size());
        return sortedWords;


    }

    public void writeToFile(String file, Map<Integer, List<String>> values)
    {
        try{

            PrintWriter printer = new PrintWriter(file, "UTF-8");

            System.out.println(values.size());
            for(Map.Entry<Integer, List<String>> value: values.entrySet())
            {
                    printer.println("<" + value.getKey() + ", " + value.getValue().toString() + ">");

                System.out.println("<" + value.getKey() + ", " + value.getValue().toString() + ">");

            }

            printer.close();;

        }

        catch(Exception exe)
        {
            System.out.println("failed");
            System.out.println(exe.getMessage());
            System.out.println(exe.getStackTrace());
        }

    }


    public String readPDF(File file)
    {
        try{



            PDFParser parser = new PDFParser(new FileInputStream(file));
            parser.parse();
            COSDocument cosDocument = parser.getDocument();
            PDDocument pdDocument = new PDDocument(cosDocument);



            PDFTextStripper textStripper = new PDFTextStripper();
            String text = textStripper.getText(pdDocument);


            cosDocument.close();;
            pdDocument.close();;


            return text;

        }

        catch(Exception exe)
        {
            System.out.println("errored");
            System.out.println(exe.getMessage());
            System.out.println(exe.getStackTrace());
        }

        return null;
    }


}
